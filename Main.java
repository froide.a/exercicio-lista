package br.com.itau;

public class Main {

    public static void main(String[] args) {
        Circulo circulo = new Circulo(5);
        System.out.println(circulo.getArea());

        Retangulo retangulo = new Retangulo(10, 2);
        System.out.println(retangulo.getArea());

    }
}
