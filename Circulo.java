package br.com.itau;

public class Circulo implements Forma {
    double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public double getArea() {
        return raio * 2;
    }
}
