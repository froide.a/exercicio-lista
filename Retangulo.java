package br.com.itau;

public class Retangulo implements Forma {
    double altura;
    double largura;

    public Retangulo(double altura, double largura) {
        this.altura = altura;
        this.largura = largura;
    }

    @Override
    public double getArea() {
        return altura * largura;
    }
}
